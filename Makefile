CC = clang
CXX = clang++
#CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native -g -pg
CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native
CXXFLAGS = $(CCFLAGS)

DEPS = Makefile
OBJ = obj

$(OBJ)/%.o: %.c %.h $(DEPS)
	mkdir -p $(OBJ)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(OBJ)/%.o: %.cpp %.hpp $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

BLOSSOM5_OBJECTS = blossom5/PM*.o blossom5/MinCost/MinCost.o

all: p01 p02

p02: p02.cpp $(OBJ)/fib_heap.o $(OBJ)/Graph.o $(OBJ)/ShortestPaths.o $(BLOSSOM5_OBJECTS)
	$(CXX) $(CXXFLAGS) p02.cpp $(OBJ)/fib_heap.o $(OBJ)/Graph.o $(OBJ)/ShortestPaths.o $(BLOSSOM5_OBJECTS) -o p02

p01: p01.c $(OBJ)/util.o $(OBJ)/graph.o $(OBJ)/matching.o $(OBJ)/special_blossom_forest.o
	$(CC) $(CCFLAGS) p01.c $(OBJ)/util.o $(OBJ)/graph.o $(OBJ)/matching.o $(OBJ)/special_blossom_forest.o -o p01

.PHONY: clean bin_clean obj_clean
clean: bin_clean obj_clean
bin_clean:
	rm -fv p01 p02
obj_clean:
	rm -rfv $(OBJ)/*
