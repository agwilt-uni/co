#ifndef MATCHING_H
#define MATCHING_H

#include "graph.h"

long *create_empty_matching(long num_vertices);
long *create_matching_from_subgraph(const struct graph *M, const struct graph *G);

long num_matching_edges(const long *mu, long num_vertices);

void unmatch(long *mu, long x, long y);
void match(long *mu, long x, long y);

void greedy_maximise(const struct graph *G, long *mu);

void matching_print_dimacs(FILE *stream, const long *mu, long num_vertices);

#endif // MATCHING_H
