#include "blossom5/PerfectMatching.h"
#include "Graph.hpp"
#include "ShortestPaths.hpp"

#include <cstring>
#include <iostream>
#include <stack>
#include <vector>

std::vector<char> compute_join(
	Graph const& G,
	ShortestPaths& shortest_paths,
	EdgeCost const& gamma,
	long const& lambda
)
{
	// compute negative Edges and Vertices with odd degrees with those edges
	std::vector<int> neg_vert_degrees(G.num_vertices(), 0);
	std::vector<long> negative_edges;
	for (int i = 0; i < G.num_edges(); ++i) {
		if (lambda*G.edge(i).cost - gamma < 0) {
			++neg_vert_degrees[G.edges()[i].x];
			++neg_vert_degrees[G.edges()[i].y];
			negative_edges.push_back(i);
		} 
	}
	std::vector<long> Tneg;
	for (int i = 0; i < G.num_vertices(); i++) {
		if (neg_vert_degrees[i] % 2) Tneg.push_back(i);
	}
	//compute shortest paths between Tnegs, with costs |lambda*cost-gamma|
	shortest_paths.compute_shortest_paths(
		Tneg,
		[&](long const e) { return std::abs(lambda*G.edge(e).cost - gamma); }
	);
	//Compute Min-Cost perfect Matching in Tneg:
	//TODO: try 0?
	PerfectMatching K = PerfectMatching(Tneg.size(), Tneg.size()*(Tneg.size()-1) / 2); 
	for (size_t i = 0; i < Tneg.size(); ++i)
		for (size_t j = i+1; j < Tneg.size(); ++j)
			if (shortest_paths.dist(Tneg[i], Tneg[j]) < COST_INFTY)
				K.AddEdge(i, j, shortest_paths.dist(Tneg[i],Tneg[j]));
	K.Solve();

	//build Join 
	std::vector<char> J(G.num_edges(), false);
	for (size_t i = 0; i < Tneg.size(); ++i) {
		const size_t j = K.GetMatch(i);
		if (j < i) continue;
		// traverse the shortest paths and sym.diff them onto J
		const long w = Tneg[j];
		for (long v = Tneg[i]; v != w;
				v = G.edge(shortest_paths.prev(w, v)).other_endpoint_from(v)) {
			J[shortest_paths.prev(w, v)] = not J[shortest_paths.prev(w, v)];
		}
	}

	//j sym diff Jneg
	for (auto i : negative_edges) {
		J[i] = not J[i];
	}

	return J;
}

void update_gamma_lambda(
	Graph const& G,
	ShortestPaths& shortest_paths,
	EdgeCost &gamma,
	long &lambda
)
{
	auto const J = compute_join(G, shortest_paths, gamma, lambda);

	gamma = lambda = 0;
	for (int i = 0; i < G.num_edges(); ++i) {
		if (J[i]) {
			++lambda;
			gamma += G.edge(i).cost;
		}
	}
}

Graph find_cycle(Graph const& G, std::vector<char> const&& allowed)
{
	Graph C(G.num_vertices());

	// edge, dist_to_root, visited
	struct TreeEntry {
		long edge;
		long dist_to_root;
		bool visited;
	};
	std::vector<TreeEntry> tree(G.num_vertices(), {0, 0, false});

	long r = 0;
	std::stack<long> stack;

	while (r < G.num_vertices()) {
		stack.push(r);
		tree[r] = {NO_EDGE, 0, true};
		while (not stack.empty()) {
			long v = stack.top();
			stack.pop();
			for (auto const& e : G.vertex(v).neighbours()) {
				if (e == tree[v].edge or not allowed[e]) { continue; }

				long w = G.edge(e).other_endpoint_from(v);
				if (tree[w].visited) {
					C.add_edge(G.edge(e));
					while (v != w) {
						if (tree[v].dist_to_root > tree[w].dist_to_root) {
							C.add_edge(G.edge(tree[v].edge));
							v = G.edge(tree[v].edge).other_endpoint_from(v);
						} else {
							C.add_edge(G.edge(tree[w].edge));
							w = G.edge(tree[w].edge).other_endpoint_from(w);
						}
					}
					goto return_cycle;
				} else {
					stack.push(w);
					tree[w] = {e, tree[v].dist_to_root+1, true};
				}
			}
		}
		while (r < G.num_vertices() and tree[r].visited) ++r;
	}

return_cycle:
	return C;
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Usage: %s input_graph output_graph\n", argv[0]);
		return 1;
	}

	FILE *input_file = strcmp(argv[1], "-") ? fopen(argv[1], "r") : stdin;
	Graph G(input_file);
	if (strcmp(argv[1], "-"))
		fclose(input_file);

	long gamma = G.max_edge_cost();
	long lambda = 1;

	//needed to check, whether c'(J)=0
	long oldgamma = -1;
	long oldlambda = -1;

	//needed to find cycle in the previous Join
	long superoldgamma = -1;
	long superoldlambda = -1;

	ShortestPaths shortest_paths(G);

	//update lambda and gamma until we are done
	do {
		superoldgamma = oldgamma;
		superoldlambda = oldlambda;
		oldgamma = gamma;
		oldlambda = lambda;
		update_gamma_lambda(G, shortest_paths, gamma, lambda);
	} while (lambda != 0 and oldgamma * lambda != gamma * oldlambda);

	FILE *output_file = strcmp(argv[2], "-") ? fopen(argv[2], "w") : stdout;

	if (superoldlambda > 0) {
		find_cycle(
			G,
			compute_join(
				G,
				shortest_paths,
				superoldgamma,
				superoldlambda
			) //any circle in J = argmin(c'(J)) is  min mean circle
		).output_dimacs(output_file);
	} else {
		find_cycle(G, std::vector<char>(G.num_vertices(), true)).output_dimacs(output_file);
	}

	if (strcmp(argv[2], "-"))
		fclose(output_file);

	return 0;
}
