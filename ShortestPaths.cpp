#include "ShortestPaths.hpp"

#include <cstdlib>
#include <iostream>

extern "C" {
#include "fib_heap.h"
}

ShortestPaths::ShortestPaths(Graph const& G) : _G{G}, _n{G.num_vertices()}
{
	_entries = new Entry[_n * _n];
	reset_distances();
}

void ShortestPaths::reset_distances()
{
	for (long i=0; i<_n*_n; ++i)
		_entries[i] = {COST_INFTY, NO_EDGE};
	for (long v=0; v<_n; ++v)
		_entries[(_n+1) * v].dist = 0;
}

void ShortestPaths::compute_shortest_paths(
	std::vector<long> const& S,
	std::function<long(long)> const& cost
)
{
	reset_distances();

	for (auto const& r : S) {
		// compute shortest-paths-tree from r

		std::vector<char> visited(_G.num_vertices(), false);
		struct fib_node **node = new struct fib_node *[_G.num_vertices()]();
		struct fib_heap heap{0, nullptr};

		visited[r] = true;
		node[r] = fib_heap_insert(&heap, r, 0);

		struct fib_node *node_addr;
		while ((node_addr = fib_heap_extract_min(&heap))) {
			long const v = node_addr->val;

			for (auto const& e : _G.vertex(v).neighbours()) {
				long const w = _G.edge(e).other_endpoint_from(v);
				if (not visited[w]) {
					visited[w] = true;
					_entries[_n*r + w] = {dist(r, v) + cost(e), e};
					node[w] = fib_heap_insert(&heap, w, node_addr->key + cost(e));
				} else if (node[w]->key > node_addr->key + cost(e)) {
					fib_heap_decrease_key(&heap, node[w], node_addr->key + cost(e));
					_entries[_n*r + w] = {dist(r, v) + cost(e), e};
				}
			}
		}

		if (heap.b) free(heap.b);
		for (int i=0; i<_G.num_vertices(); ++i) {
			if (node[i]) free(node[i]);
		}
		delete[] node;
	}
}
