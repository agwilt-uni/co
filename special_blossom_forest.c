#include "special_blossom_forest.h"

#include <stdio.h>
#include <stdlib.h>

/*
 * static declarations
 */

static long next_x(
	const struct special_blossom_forest *forest,
	const bool *scanned,
	long x
);

// returns NO_VERTEX if none exists
static long find_suitable_neighbour(const struct special_blossom_forest *forest, long x);

static long first_common_base_vertex_on_paths(
	const struct special_blossom_forest *forest,
	long x,
	long y
);

// WARNING: Assumes that v is already an outer blossom base
static inline long next_outer_base(const struct special_blossom_forest *forest, long v);

static void change_phi_for_shrink(struct special_blossom_forest *forest, long z, long r);

// send all vertices v, _blossom_base[v] in P(z) to new base r
static void send_to_new_blossom(struct special_blossom_forest *forest, long z, long r);

static void move_over_for_augment(struct special_blossom_forest *forest, long z);

// reset tree with root r
static void nuke_tree(struct special_blossom_forest *forest, long r);

static inline bool is_outer_vertex(const struct special_blossom_forest *forest, long x);
static inline bool is_out_of_forest(const struct special_blossom_forest *forest, long x);

/*
 * definitions
 */

struct special_blossom_forest create_special_blossom_forest_from_subgraph(
	const struct graph *M,
	const struct graph *G
)
{
	long const n = G->num_vertices;

	struct special_blossom_forest forest = {
		.mu = create_matching_from_subgraph(M, G),
		.phi = malloc(n * sizeof(long)),
		.blossom_base = malloc(n * sizeof(long)),
		.blossom_from = calloc(n, sizeof(struct vertex)),
		.root = malloc(n * sizeof(long)),
		.dist_to_root = calloc(n, sizeof(long)),
		.tree_from = calloc(n, sizeof(struct vertex)),
		.G = G
	};

	for (long v = 0; v < n; ++v) {
		forest.phi[v] = v;
		forest.blossom_base[v] = v;
		add_neighbour(forest.blossom_from + v, v);
		forest.root[v] = v;
		add_neighbour(forest.tree_from + v, v);
	}

	return forest;
}

void free_special_blossom_forest(struct special_blossom_forest *forest)
{
	free(forest->mu);
	free(forest->phi);
	free(forest->blossom_base);
	for (long v = 0; v < forest->G->num_vertices; ++v)
		free(forest->blossom_from[v].neighbours);
	free(forest->blossom_from);
	free(forest->root);
	free(forest->dist_to_root);
	for (long v = 0; v < forest->G->num_vertices; ++v)
		free(forest->tree_from[v].neighbours);
	free(forest->tree_from);

	*forest = (struct special_blossom_forest) { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
}

long next_x(
	const struct special_blossom_forest *forest,
	const bool *scanned,
	long x
)
{
	for (; x < forest->G->num_vertices; ++x) {
		if ((! scanned[x]) && is_outer_vertex(forest, x)) { return x; }
	}
	for (x = 0; x < forest->G->num_vertices; ++x) {
		if ((! scanned[x]) && is_outer_vertex(forest, x)) { return x; }
	}
	return NO_VERTEX;
}

void edmonds_matching_algorithm(
	struct special_blossom_forest *forest,
	const volatile sig_atomic_t *exit_prematurely
)
{
	long const n = forest->G->num_vertices;
	bool *scanned = calloc(n, sizeof(bool));
	long *scanned_vertices = malloc(n * sizeof(long));
	size_t num_scanned = 0;

	long num_edges = num_matching_edges(forest->mu, n);

	long x = -1;
	while ((x = next_x(forest, scanned, x+1)) != NO_VERTEX) {

		if (*exit_prematurely)
			break;

		long y;
		while ((y = find_suitable_neighbour(forest, x)) != NO_VERTEX) {
			if (is_out_of_forest(forest, y)) {
				// grow
				const long mu_y = forest->mu[y];
				forest->phi[y] = x;
				forest->root[y] = forest->root[x];
				forest->root[mu_y] = forest->root[x];
				forest->dist_to_root[y] = forest->dist_to_root[x] + 1;
				forest->dist_to_root[mu_y] = forest->dist_to_root[x] + 2;
				forest->tree_from[y].num_neighbours = 0;
				forest->tree_from[mu_y].num_neighbours = 0;
				add_neighbour(forest->tree_from + forest->root[x], y);
				add_neighbour(forest->tree_from + forest->root[x], mu_y);
				continue;
			} else if (forest->root[x] != forest->root[y]) {
				// augment
				move_over_for_augment(forest, x);
				move_over_for_augment(forest, y);
				match(forest->mu, x, y);
				// reset forest
				while (num_scanned > 0)
					scanned[scanned_vertices[--num_scanned]] = false;
				nuke_tree(forest, forest->root[x]);
				nuke_tree(forest, forest->root[y]);
				++num_edges;
				break; // continue in for-loop
			} else {
				// shrink
				long const r = first_common_base_vertex_on_paths(forest, x, y);
				change_phi_for_shrink(forest, x, r);
				change_phi_for_shrink(forest, y, r);
				if (forest->blossom_base[x] != r) { forest->phi[x] = y; }
				if (forest->blossom_base[y] != r) { forest->phi[y] = x; }
				send_to_new_blossom(forest, x, r);
				send_to_new_blossom(forest, y, r);
			}
		}
		scanned[x] = true;
		scanned_vertices[num_scanned++] = x;
	}

	free(scanned_vertices);
	free(scanned);
}

long find_suitable_neighbour(const struct special_blossom_forest *forest, const long x)
{
	for (size_t i = 0; i < forest->G->vertices[x].num_neighbours; ++i) {
		const long y = forest->G->vertices[x].neighbours[i];
		if (is_out_of_forest(forest, y)) return y;
		if (is_outer_vertex(forest, y) &&
		    (forest->blossom_base[x] != forest->blossom_base[y])) return y;
	}
	return NO_VERTEX;
}

long first_common_base_vertex_on_paths(
	const struct special_blossom_forest *forest,
	long x,
	long y
)
{
	x = forest->blossom_base[x];
	y = forest->blossom_base[y];
	while (x != y) {
		if (forest->dist_to_root[x] > forest->dist_to_root[y])
			x = next_outer_base(forest, x);
		else
			y = next_outer_base(forest, y);
	}
	return x;
}

inline long next_outer_base(const struct special_blossom_forest *forest, long const v)
{
	return forest->blossom_base[forest->phi[forest->mu[v]]];
}

void change_phi_for_shrink(struct special_blossom_forest *forest, long z, long const r)
{
	if (r == z) { return; }
	long mu_z;
	while (mu_z = forest->mu[z], z = forest->phi[mu_z], forest->blossom_base[z] != r)
		forest->phi[z] = mu_z;
}

void send_to_new_blossom(struct special_blossom_forest *forest, long const z, long const r)
{
	for (long b = forest->blossom_base[z]; b != r; b = next_outer_base(forest, b)) {
		for (size_t i = 0; i < forest->blossom_from[b].num_neighbours; ++i) {
			const long v = forest->blossom_from[b].neighbours[i];
			forest->blossom_base[v] = r;
			add_neighbour(forest->blossom_from + r, v);
		}
		forest->blossom_from[b].num_neighbours = 0;

		const long mu_b = forest->mu[b];
		forest->blossom_base[mu_b] = r;
		add_neighbour(forest->blossom_from + r, mu_b);
		forest->blossom_from[mu_b].num_neighbours = 0;
	}
}

void move_over_for_augment(struct special_blossom_forest *forest, long z)
{
	if (z == forest->root[z]) { return; }

	long next = forest->mu[z];
	unmatch(forest->mu, z, forest->mu[z]);
	do {
		// z is always a vertex with odd distance to top, i.e. inner if not in blossom
		z = next;
		next = forest->mu[forest->phi[z]];
		unmatch(forest->mu, next, forest->phi[z]);
		match(forest->mu, z, forest->phi[z]);
	} while (next != forest->root[z]);
}

void nuke_tree(struct special_blossom_forest *forest, long const r)
{
	for (size_t i = 0; i < forest->tree_from[r].num_neighbours; ++i) {
		long const v = forest->tree_from[r].neighbours[i];
		forest->phi[v] = v;
		forest->blossom_base[v] = v;
		// O.K. since was initialized with length 1:
		forest->blossom_from[v].neighbours[0] = v;
		forest->blossom_from[v].num_neighbours = 1;
		if (v != r) {
			forest->root[v] = v;
			forest->dist_to_root[v] = 0;
			forest->tree_from[v].neighbours[0] = v;
			forest->tree_from[v].num_neighbours = 1;
		}
	}
	forest->root[r] = r;
	forest->tree_from[r].neighbours[0] = r;
	forest->tree_from[r].num_neighbours = 1;
}

inline bool is_outer_vertex(const struct special_blossom_forest *forest, long const x)
{
	return (forest->mu[x] == x) || (forest->phi[forest->mu[x]] != forest->mu[x]);
}

inline bool is_out_of_forest(const struct special_blossom_forest *forest, long const x)
{
	return (forest->mu[x] != x) &&
	       (forest->phi[x] == x) &&
	       (forest->phi[forest->mu[x]] == forest->mu[x]);
}
