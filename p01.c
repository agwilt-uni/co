#include "graph.h"
#include "special_blossom_forest.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

static const char *stdin_file_name = "-";
static volatile sig_atomic_t exit_prematurely = 0;

void graceful_terminate(int signum)
{
	if (exit_prematurely) exit(130);

	fprintf(stderr, " Returning suboptimal matching after %d received.\n", signum);
	exit_prematurely = 1;
}

bool is_real_file(char *filename)
{
	return ((filename != NULL) && (strcmp(filename, "-") != 0));
}

// filename == NULL --> return graph with no edges, n vertices
// *filename == "-" --> read graph from stdin
// else read graph from filename
struct graph graph_from_possible_filename(const char *filename, long const num_vertices)
{
	if (filename == NULL) {
		return (struct graph) {
			.vertices = calloc(num_vertices, sizeof(struct vertex)),
			.num_vertices = num_vertices,
			.num_edges = 0
		};
	} else if (strcmp(filename, stdin_file_name)) {
		FILE *graph_file = fopen(filename, "r");
		struct graph G = read_graph_from_dimacs(graph_file);
		fclose(graph_file);
		return G;
	} else {
		return read_graph_from_dimacs(stdin);
	}
}

int main(int argc, char *argv[])
{
	// Parse arguments
	const char *graph_file_name = NULL;
	const char *hint_file_name = NULL;

	for (int i=1; i<argc; ++i) {
		if (strcmp(argv[i], "--help") == 0) {
			// print help message
			printf("Usage: %s [--graph] file1.dmx [[--hint] file2.dmx]\n", argv[0]);
			return 0;
		} else if ((i < argc-1) && (strcmp(argv[i], "--graph") == 0)) {
			// graph filename
			if (graph_file_name != NULL) {
				fprintf(
					stderr,
					"%s: Error: Please supply at most one input graph.\n",
					argv[0]
				);
				return 1;
			}
			graph_file_name = argv[++i];
		} else if ((i < argc-1) && (strcmp(argv[i], "--hint") == 0)) {
			// hint-graph filename
			if (hint_file_name != NULL) {
				fprintf(stderr, "%s: Error: Please supply at most one hint graph.\n", argv[0]);
				return 1;
			}
			hint_file_name = argv[++i];
		} else if (graph_file_name == NULL) {
			// graph filename, without "--graph"
			graph_file_name = argv[i];
		} else if (hint_file_name == NULL) {
			// hint filename, without "--hint"
			hint_file_name = argv[i];
		} else {
			fprintf(stderr, "Usage: %s [--graph] file1.dmx [[--hint] file2.dmx]\n", argv[0]);
			return 1;
		}
	}
	if (graph_file_name == NULL) {
		fprintf(stderr, "%s: Error: Please supply an input graph.\n", argv[0]);
		return 1;
	}

	struct graph G = graph_from_possible_filename(graph_file_name, NO_VERTEX);
	struct graph M = graph_from_possible_filename(hint_file_name, G.num_vertices);

	signal(SIGINT, graceful_terminate);

	struct special_blossom_forest blossom_forest =
		create_special_blossom_forest_from_subgraph(&M, &G);
	free_graph(&M);
	fprintf(
		stderr,
		"Matching size before greedy: %ld\n",
		num_matching_edges(blossom_forest.mu, G.num_vertices)
	);

	greedy_maximise(&G, blossom_forest.mu);
	fprintf(
		stderr,
		"Matching size after greedy:  %ld\n",
		num_matching_edges(blossom_forest.mu, G.num_vertices)
	);

	edmonds_matching_algorithm(&blossom_forest, &exit_prematurely);
	if (! exit_prematurely) {
		fprintf(
			stderr,
			"Max cardinality matching:    %ld\n",
			num_matching_edges(blossom_forest.mu, G.num_vertices)
		);
	}
	matching_print_dimacs(stdout, blossom_forest.mu, G.num_vertices);

	free_special_blossom_forest(&blossom_forest);
	free_graph(&G);

	return exit_prematurely ? 124 : 0;
}
