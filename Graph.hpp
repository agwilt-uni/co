#ifndef GRAPH_H
#define GRAPH_H

#include <utility>
#include <vector>
#include <cstdio>
#include <limits>

#define NO_VERTEX -1
#define NO_EDGE -1
using EdgeCost = long;
constexpr EdgeCost COST_INFTY = std::numeric_limits<EdgeCost>::max();

class Vertex {
	friend class Graph;

public:
	Vertex() : _neighbours() {}
	std::vector<long> const& neighbours() const { return _neighbours; }
	long degree() const { return _neighbours.size(); }

private:
	std::vector<long> _neighbours;
};

class Edge {
public:
	 Edge(long const x, long const y, EdgeCost const cost) : x{x}, y{y}, cost{cost} {}
	 long other_endpoint_from(long const v) const { return x+y-v; }

public:
	const long x;
	const long y;
	EdgeCost cost;
};

class Graph {
public:
	Graph(long num_vertices=0) : _vertices(num_vertices), _edges() {}
	Graph(FILE *stream); // read from DIMACS

	std::vector<Vertex> const& vertices() const { return _vertices; }
	std::vector<Edge> const& edges() const { return _edges; }
	long num_vertices() const { return _vertices.size(); }
	long num_edges() const { return _edges.size(); }
	const Vertex &vertex(long const id) const { return _vertices[id]; }
	const Edge &edge(long const id) const { return _edges[id]; }
	long add_edge(Edge edge);
	// warning: O(|delta(x)|) running time
	bool adjacent(long x, long y) const;
	EdgeCost max_edge_cost() const;

	void output_dimacs(FILE *stream);

private:
	std::vector<Vertex> _vertices;
	std::vector<Edge> _edges;
};

#endif // GRAPH_H
