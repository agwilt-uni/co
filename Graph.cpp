#include "Graph.hpp"
#include <cstdlib>


bool Graph::adjacent(long const x, long const y) const
{
	for (auto const& i : vertex(x).neighbours())
		if (edge(i).other_endpoint_from(x) == y)
			return true;
	return false;
}



EdgeCost Graph::max_edge_cost() const
{
	EdgeCost max = -COST_INFTY;
	for (auto const& e : _edges)
		if (e.cost >= max)
			max = e.cost;
	return max;
}


long Graph::add_edge(Edge const edge)
{
	const long edge_id = _edges.size();
	_edges.push_back(edge);
	_vertices[edge.x]._neighbours.push_back(edge_id);
	_vertices[edge.y]._neighbours.push_back(edge_id);
	return edge_id;
}

Graph::Graph(FILE *stream) : Graph()
{
	if (stream == NULL) {
		fprintf(stderr, "Invalid file stream.\n");
		exit(1);
	}

	// Navigate to first "p edge ....." line
	while (getc(stream) != 'p') {
		while (getc(stream) != '\n');
	}

	long n, m;
	if (fscanf(stream, " edge %ld %ld\n", &n, &m) != 2) {
		fprintf(stderr, "Bad first line!\n");
		exit(1);
	}
	_vertices = std::vector<Vertex>(n);
	_edges.reserve(m);

	char *line = NULL;
	size_t len = 0;
	for (long i=0; i < m; ++i) {
		if (getline(&line, &len, stream) == 1) {
			fprintf(stderr, "Wrong number of edges!\n");
			exit(1);
		}
		if (line[0] == 'c') { continue; }

		long x, y;
		long cost;
		if (sscanf(line, "e %ld %ld %ld\n", &x, &y, &cost) != 3) {
			fprintf(stderr, "ERROR: Badly formatted edge\n");
			exit(1);
		}
		if ((x == y) || (x < 1) || (y < 1) || (x > n) || (y > n)) {
			fprintf(stderr, "ERROR: Bad edge: %ld %ld %ld\n", x, y, cost);
			exit(1);
		}

		// convert from DIMACS
		add_edge({x-1, y-1, cost});
	}
	free(line);
}

void Graph::output_dimacs(FILE *stream)
{
	fprintf(stream, "p edge %ld %ld\n", num_vertices(), num_edges());
	for (auto const& e : _edges)
		fprintf(stream, "e %ld %ld %ld\n", e.x, e.y, e.cost);
}
