#ifndef SPECIAL_BLOSSOM_FOREST_H
#define SPECIAL_BLOSSOM_FOREST_H

#include <signal.h>
#include <stdbool.h>
#include "graph.h"
#include "matching.h"

struct special_blossom_forest {
	long *mu;
	long *phi;
	long *blossom_base;
	// array of vertices, whose neighbours are the blossom members
	struct vertex *blossom_from;
	long *root;
	long *dist_to_root; // more-or-less length of P(v)
	struct vertex *tree_from;
	const struct graph *G;
};

void free_special_blossom_forest(struct special_blossom_forest *forest);

struct special_blossom_forest create_special_blossom_forest_from_subgraph(
	const struct graph *M,
	const struct graph *G
);

void edmonds_matching_algorithm(
	struct special_blossom_forest *forest,
	const volatile sig_atomic_t *exit_prematurely
);

#endif // SPECIAL_BLOSSOM_FOREST_H
