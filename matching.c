#include "matching.h"

#include <stdio.h>
#include <stdlib.h>

long *create_empty_matching(long num_vertices)
{
	long *const mu = malloc(num_vertices * sizeof(long));
	for (long v = 0; v < num_vertices; ++v)
		mu[v] = v;
	return mu;
}

long *create_matching_from_subgraph(const struct graph *M, const struct graph *G)
{
	if (M->num_vertices != G->num_vertices) {
		fprintf(
			stderr,
			"ERROR: matching-graph has different number of vertices from G\n"
		);
		exit(1);
	}
	long *const mu = malloc(M->num_vertices * sizeof(long));
	for (long v_id = 0; v_id < M->num_vertices; ++v_id) {
		const struct vertex *const v = M->vertices + v_id;
		switch (v->num_neighbours) {
			case 1:
				if (!vertices_adjacent(G->vertices+v_id, v->neighbours[0])) {
					fprintf(
						stderr,
						"ERROR: matching-graph not a subgraph of G\n"
					);
					exit(1);
				}
				mu[v_id] = v->neighbours[0];
				break;
			case 0:
				mu[v_id] = v_id;
				break;
			default:
				fprintf(stderr, "ERROR: matching-graph not a matching\n");
				exit(1);
		}
	}
	return mu;
}

long num_matching_edges(const long *mu, long const num_vertices)
{
	long ans = 0;
	for (long v = 0; v < num_vertices; ++v)
		ans += (mu[v] > v);
	return ans;
}

void unmatch(long *mu, long const x, long const y)
{
	mu[x] = x;
	mu[y] = y;
}

void match(long *mu, long const x, long const y)
{
	mu[x] = y;
	mu[y] = x;
}

void greedy_maximise(const struct graph *G, long *mu)
{
	for (long v_id = 0; v_id < G->num_vertices; ++v_id) {
		if (mu[v_id] != v_id) { continue; }
		for (size_t i = 0; i < G->vertices[v_id].num_neighbours; ++i) {
			long const w_id = G->vertices[v_id].neighbours[i];
			if (mu[w_id] == w_id) {
				mu[v_id] = w_id;
				mu[w_id] = v_id;
				break;
			}
		}
	}
}

void matching_print_dimacs(FILE *stream, const long *mu, long const num_vertices)
{
	long const num_edges = num_matching_edges(mu, num_vertices);
	fprintf(stream, "p edge %ld %ld\n", num_vertices, num_edges);
	for (long v = 0; v < num_vertices; ++v)
		if (mu[v] > v)
			fprintf(stream, "e %ld %ld\n", v+1, mu[v]+1);
}
