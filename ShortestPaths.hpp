#ifndef SHORTEST_PATHS_H
#define SHORTEST_PATHS_H

#include "Graph.hpp"

#include <functional>

class ShortestPaths {
public:
	struct Entry {
		EdgeCost dist;
		long edge_id;
		constexpr bool operator<(Entry const& rhs) const { return dist < rhs.dist; }
	};
public:
	explicit ShortestPaths(Graph const& G);
	~ShortestPaths() { if (_n) delete[] _entries; }

	void reset_distances();
	// compute shortest paths using non-negative cost functions
	void compute_shortest_paths(
		std::vector<long> const& S,
		std::function<long(long)> const& cost
	);

	constexpr Entry	entry(long const x, long const y) const { return _entries[_n*x + y]; }

	// distance COST_INFTY means no path (yet) found
	constexpr EdgeCost dist(long const x, long const y) const { return entry(x, y).dist; }

	// prev not well-defined where dist[x][y] = COST_INFTY
	constexpr long prev(long const x, long const y) const { return entry(x, y).edge_id; }

private:
	Graph const& _G;
	long const _n;
	Entry *_entries;
};

#endif // SHORTEST_PATHS_H
