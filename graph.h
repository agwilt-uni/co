#ifndef GRAPH_H
#define GRAPH_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define NO_VERTEX -1

struct vertex {
	long *neighbours;
	size_t num_neighbours;
	size_t _max_num_neighbours;
};

void add_neighbour(struct vertex *v, long w);

// warning: O(|delta(x)|) running time
bool vertices_adjacent(const struct vertex *x, long y);

struct graph {
	struct vertex *vertices;
	long num_vertices;
	long num_edges;
};

void free_graph(struct graph *G);

struct graph read_graph_from_dimacs(FILE *stream);

void graph_add_edge(struct graph *G, long x, long y);

#endif // GRAPH_H
